import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: Color(0xFF152636),
      //   elevation: 0,
      // ),
      backgroundColor: Color(0xFF1e2e3e),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //Image(image: AssetImage('images/Ekhon-Charge--White-Logo.png'),width: 200,height: 200,),

            // Padding(
            //   padding: const EdgeInsets.only(top: 26.0),
            //   child: Image(image: AssetImage('images/Ekhon-Charge--White-Logo.png',),height: 100,width: double.infinity),
            // ),
            // Container(
            //   //color: const Color(0x152636),
            //   foregroundDecoration: BoxDecoration(color: Color(0x152636)),
            //
            //
            //   //color: Colors.red,
            //   width: double.infinity,
            //   height: 100,
            //   // child: Image(
            //   //   image: AssetImage(
            //   //     'images/Ekhon-Charge--White-Logo.png',
            //   //   ),
            //   // ),
            // ),
            Container(

              width: double.infinity,
              height: 100,
              decoration: BoxDecoration(
                color: Color(0xFF152636),
              ),
              child: Image.asset('images/Ekhon-Charge--White-Logo.png'),
            ),
            Image(
              image: AssetImage('images/carpic.png'),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Container(
                    height: 2,
                    width: 50,
                    color: Colors.white,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    'Welcome To',
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Ekhon Charge',
                    style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w500,
                        color: Colors.white),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  ElevatedButton(
                    onPressed: null,

                    child: Text('Sign In',style: TextStyle(color: Colors.white),),
                    style: ElevatedButton.styleFrom(elevation: 3,minimumSize: Size(double.infinity,30),side: BorderSide(color: Colors.green,width: 2),),

                  ),
                  // SizedBox(height: 5,),
                  ElevatedButton(
                    onPressed: null,

                    child: Text('Sign In',style: TextStyle(color: Colors.white),),
                    style: ElevatedButton.styleFrom(elevation: 3,minimumSize: Size(double.infinity,30),side: BorderSide(color: Colors.green,width: 2),),

                  ),
                  Image.asset('images/sentinellogo.png',width: 250,height: 75,)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
